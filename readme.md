<p>JSON format:</p>

[
  {
    "id": 1,
    "name": "Toni",
    "description": "Fluffy and funny",
    "breed_id": 1,
    "category_id": 1,
    "birth_date": "2013-09-03",
    "price": "0.00",
    "created_at": null,
    "updated_at": null
  },
  {
    "id": 2,
    "name": "Sami",
    "description": "Not the prettiest one",
    "breed_id": 2,
    "category_id": 2,
    "birth_date": "2012-02-01",
    "price": "0.00",
    "created_at": null,
    "updated_at": null
  }
] 

<p>API calls:</p>

<p>READ:</p>
<p>GET::http://localhost/PonyShop/public/index.php/api/v1/ponies/{id?}</p>
<p>GET::http://localhost/PonyShop/public/index.php/api/v1/categories/{id?}</p>
<p>GET::http://localhost/PonyShop/public/index.php/api/v1/breeds/{id?}</p>

<p>CREATE:</p>
<p>POST::http://localhost/PonyShop/public/index.php/api/v1/ponies</p>
<p>POST::http://localhost/PonyShop/public/index.php/api/v1/categories</p>
<p>POST::http://localhost/PonyShop/public/index.php/api/v1/breeds</p>

<p>UPDATE:</p>
<p>POST::http://localhost/PonyShop/public/index.php/api/v1/ponies/{id}</p>
<p>POST::http://localhost/PonyShop/public/index.php/api/v1/categories/{id}</p>
<p>POST::http://localhost/PonyShop/public/index.php/api/v1/breeds/{id}</p>

<p>DELETE:</p>
<p>DELETE::http://localhost/PonyShop/public/index.php/api/v1/ponies/{id}</p>
<p>DELETE::http://localhost/PonyShop/public/index.php/api/v1/categories/{id}</p>
<p>DELETE::http://localhost/PonyShop/public/index.php/api/v1/breeds/{id}</p>

