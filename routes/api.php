<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

// API routes...
// http://localhost/PonyShop/public/index.php/api/v1/ponies/1 is the correct url

Route::get('/v1/categories/{id?}', 'Categories@index');
Route::post('/v1/categories', 'Categories@store');
Route::post('/v1/categories/{id}', 'Categories@update');
Route::delete('/v1/categories/{id}', 'Categories@destroy');

Route::get('/v1/ponies/{id?}', 'Ponies@index');
Route::post('/v1/ponies', 'Ponies@store');
Route::post('/v1/ponies/{id}', 'Ponies@update');
Route::delete('/v1/ponies/{id}', 'Ponies@destroy');

Route::get('/v1/breeds/{id?}', 'Breeds@index');
Route::post('/v1/breeds', 'Breeds@store');
Route::post('/v1/breeds/{id}', 'Breeds@update');
Route::delete('/v1/breeds/{id}', 'Breeds@destroy');