<?php

use Illuminate\Database\Seeder;

class CategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('categories')->insert(['name' => 'SHOW']);
        DB::table('categories')->insert(['name' => 'RACING']);
        DB::table('categories')->insert(['name' => 'HOBBY']);
        DB::table('categories')->insert(['name' => 'FANTASY']);
    }
}
