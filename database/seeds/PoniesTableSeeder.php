<?php

use Illuminate\Database\Seeder;

class PoniesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('ponies')->insert([
            'name' => 'Toni',
            'description' => 'Fluffy and funny',
            'breed_id' => 1,
            'category_id' => 1,
            'birth_date' => '2013-09-03',
        ]);
        DB::table('ponies')->insert([
            'name' => 'Sami',
            'description' => 'Not the prettiest one',
            'breed_id' => 2,
            'category_id' => 2,
            'birth_date' => '2012-02-01',
        ]);
        DB::table('ponies')->insert([
            'name' => 'Ulla',
            'description' => 'Super active',
            'breed_id' => 3,
            'category_id' => 3,
            'birth_date' => '1880-10-10',
        ]);
        DB::table('ponies')->insert([
            'name' => 'Seppo',
            'description' => 'Always thirsty for rainbows',
            'breed_id' => 6,
            'category_id' => 4,
            'birth_date' => '2113-02-02',
        ]);
        DB::table('ponies')->insert([
            'name' => 'Kari',
            'description' => 'Likes carrots more than anything',
            'breed_id' => 4,
            'category_id' => 1,
            'birth_date' => '2003-01-01',
        ]);
    }
}
