<?php

use Illuminate\Database\Seeder;

class BreedsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('breeds')->insert(['name' => 'Shetland']);
        DB::table('breeds')->insert(['name' => 'Sport Pony']);
        DB::table('breeds')->insert(['name' => 'Welsh Pony']);
        DB::table('breeds')->insert(['name' => 'Dulmen']);
        DB::table('breeds')->insert(['name' => 'Dales']);
        DB::table('breeds')->insert(['name' => 'My Little Pony']);
    }
}
