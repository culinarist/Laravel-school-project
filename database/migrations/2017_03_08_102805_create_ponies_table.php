<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePoniesTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('ponies', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name',75)->unique();
            $table->text('description')->nullable();
            $table->integer('breed_id');
            $table->integer('category_id');
            $table->date('birth_date');
            $table->timestamps();        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::drop('ponies');
    }
}
