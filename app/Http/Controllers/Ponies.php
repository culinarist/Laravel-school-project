<?php

namespace App\Http\Controllers;

use App\Pony;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class Ponies extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index($id = null) {
        if ($id == null) {
            return Pony::orderBy('id', 'asc')->get();
        } else {
            return $this->show($id);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request  $request
     * @return Response
     */
    public function store(Request $request) {

        $this->validate($request, [
            'name' => 'bail|required|max:75',
            'breed_id' => 'required',
            'category_id' => 'required',
            'birth_date' => 'required',
            'price' => 'required',
        ]);

        $pony = new Pony;

        $pony->name = $request->input('name');
        $pony->description = $request->input('description');
        $pony->breed_id = $request->input('breed_id');
        $pony->category_id = $request->input('category_id');
        $pony->birth_date = $request->input('birth_date');
        $pony->price = $request->input('price');
        $pony->save();

        return 'Pony record successfully created with id ' . $pony->id;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id) {
        return Pony::find($id);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request  $request
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request, $id) {

        $this->validate($request, [
            'name' => 'max:75',
        ]);

        $pony = Pony::find($id);

        $pony->name = $request->input('name');
        $pony->description = $request->input('description');
        $pony->breed_id = $request->input('breed_id');
        $pony->category_id = $request->input('category_id');
        $pony->birth_date = $request->input('birth_date');
        $pony->price = $request->input('price');

        $pony->save();

        return "Sucess updating pony #" . $pony->id;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy(Request $request, $id) {

        if($id != null){
            $pony = Pony::find($id);

            $pony->delete();
        } else {
            $pony = Pony::find($request->input('id'));

            $pony->delete();
        }


        return "Pony record successfully deleted #" . $request->input('id');
    }
}
