<?php

namespace App\Http\Controllers;

use App\Breed;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class Breeds extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index($id = null) {
        if ($id == null) {
            return Breed::orderBy('id', 'asc')->get();
        } else {
            return $this->show($id);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request  $request
     * @return Response
     */
    public function store(Request $request) {

        $this->validate($request, [
            'name' => 'required|unique:posts|max:75',
        ]);

        $breed = new Breed();

        $breed->name = $request->input('name');
        $breed->save();

        return 'Breed record successfully created with id ' . $breed->id;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id) {
        return Breed::find($id);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request  $request
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request, $id) {

        $this->validate($request, [
            'name' => 'max:75',
        ]);

        $pony = Breed::find($id);

        $pony->name = $request->input('name');
        $pony->save();

        return "Sucess updating breed #" . $pony->id;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy(Request $request) {

        $this->validate($request, [
            'id' => 'required',
        ]);

        $category = Breed::find($request->input('id'));

        $category->delete();

        return "Breed record successfully deleted #" . $request->input('id');
    }
}
