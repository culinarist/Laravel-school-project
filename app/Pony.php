<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pony extends BaseModel
{
    protected $primaryKey = 'id';
    protected $table = 'ponies';
    protected $fillable = array('name', 'description', 'breed_id', 'category_id', 'birth_date', 'price', 'created_at_ip', 'updated_at_ip');
    public $timestamps = false;

    public function breed()
    {
        return $this->hasOne('App\Breed');
    }

    public function category()
    {
        return $this->hasOne('App\Category');
    }
}
