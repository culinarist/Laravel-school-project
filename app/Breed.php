<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Breed extends BaseModel
{
    protected $primaryKey = 'id';
    protected $table = 'breeds';
    protected $fillable = array('name', 'created_at_ip', 'updated_at_ip');
    public $timestamps = false;
}
